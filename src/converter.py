'''
Conversion functions
'''

from _collections import OrderedDict
from os.path import basename, splitext
from pprint import pprint
import logging

from pyparsing import (alphas, alphanums, delimitedList, oneOf, Optional, Word,
    Keyword, Suppress, quotedString, pyparsing_common as ppc, Group, ParseException)

from __init__ import __appname__, __version__
from tools import get_file_type, indentlines
import public

INDENTATION = ' ' * 4


def convert_file(source, destination):
    '''
    Convert a VB6/VBA file to Python syntax

    :param str source: Source file path
    :param str destination: Destination file path (will be overwritten without consent)
    '''

    logger = logging.getLogger('convert_file')
    logger.debug('Convert %s to %s', source, destination)

    with open(source, 'r') as sourcefile:
        content = sourcefile.read()

    lines = content.split('\n')
    logger.debug('%d lines read', len(lines))

    filetype = get_file_type(source)
    if filetype in ['module']:
        new_content = convert_module(lines)
    elif filetype in ['class', 'form', 'report']:
        new_content = convert_class(lines, basename(source))

    with open(destination, 'w') as destfile:
        destfile.write(new_content)
        logger.debug('%d lines written', len(new_content.split('\n')))


def convert_module(lines, indent='', is_class_module=False):
    '''
    Convert a standard module::

        **One Func to rule them all**
        **One Func to find them**, docstrings and function/sub/property headers,
        **One Func to bring them all** in the right order,
        **and to the new code bind them** docstring/function together in a pythonic way

    This function is also used to convert the body of a class in `func:convert_class`_.

    :param list lines: Lines of the original file
    :param str indent: How shall the code been indented?
    :param bool is_class_module: Is this function part of a class module?
    :returns: Converted code
    :rtype: str
    '''

    # Combines both OrderedDicts and returns again an ordered dict
    docs = find_docstrings(lines)
    funcs = find_functions(lines, is_class_module)
    both = {**docs, **funcs}

    # Check the order of docs and functions
    pattern = []
    for item in both:
        if both[item].lstrip().startswith("'''"):
            pattern.append('doc')
        else:
            pattern.append('func')

    # Convert dict to a list to detect the pattern
    parts = [both[x] for x in both]
    new_content = ''

    if parts[0].lstrip().startswith("'''") and parts[1].lstrip().startswith("'''"):
        # There is a module docstring at the beginning
        new_content = parts[0] + '\n'

    # Now try to find a docstring right before any of the functions
    for func in funcs:
        new_content += indentlines(funcs[func], indent)
        for doc in docs:
            dstart, dend = doc
            if func == dend + 1:
                new_content += indentlines(docs[(dstart, dend)], INDENTATION) + '\n'

    return new_content


def convert_class(lines, filename):
    '''
    Convert a class module

    :param list lines: Lines of original file
    :param str filename: File name (necessary to detect the class name,
        because VBA doesn't ha a ``class`` keyword)
    :returns: Converted code
    :rtype: str
    '''

    vba_class_name = splitext(filename)[0]
    vba_class_docstring = ''

    code = """'''
Class {cname}

Converted from VBA source code by {aname} v{aversion}"
'''

class {cname}():

""".format(cname=vba_class_name, aname=__appname__, aversion=__version__)

    code += convert_module(lines, INDENTATION, True)

    return code


def convert_function(original, is_class_module=False):
    '''
    Convert VBA function (header only) to Python syntax

    :param str original: Original function header
    :param bool is_class_module: Is this function part of a class module?
    :returns: Python function header
    :rtype: str
    '''

    funcparser = get_func_parser()
    command = funcparser.parseString(original)
    func_name = command.fname
    params = [p.pname for p in command.parameters]

    # In a class module there are some special methods that have to be renamed for Python
    if is_class_module:
        if func_name == 'Class_Initialize':
            func_name = '__init__'
        elif func_name == 'Class_Terminate':
            func_name = '__del__'

        params.insert(0, 'self')

    result = 'def {name}({params}):\n'.format(
            name=func_name,
            params=', '.join(params))

    return result


def get_func_parser():
    '''
    Analyse a function line

    Thanks to [PaulMcG](https://stackoverflow.com/users/165216/paulmcg) on
    StackOverflow, who was of great help!

    :returns: pyparsing's grammar to identify and analyse a function or sub
    :rtype: pyparsing.MatchFirst
    '''
    #pylint: disable=invalid-name,line-too-long

    LPAR, RPAR, EQ = map(Suppress, "()=")
    OPTIONAL, BYREF, BYVAL, AS, FUNCTION, SUB = map(Keyword, "Optional ByRef ByVal As Function Sub".split())

    identifier = Word(alphas, alphanums + '_')
    rvalue = ppc.number() | quotedString() | identifier()
    type_expr = identifier()

    param_expr = Group(Optional(OPTIONAL('optional')) \
        +Optional(BYREF('byref') | BYVAL('byval')) \
        +identifier('pname') \
        +Optional(AS + type_expr('ptype')) \
        +Optional(EQ + rvalue('default')))

    protection = oneOf('Public Private', asKeyword=True)

    func_expr = (
        Optional(protection("protection"))
        +(FUNCTION | SUB)
        +identifier("fname")
        +Group(LPAR + delimitedList(Optional(param_expr)) + RPAR)("parameters")
        +Optional(AS + type_expr("return_type"))
        )

    return func_expr


def convert_docstring(docstring):
    '''
    Convert VBA comment block to Python syntax

    :param str docstring: the complete VBA docstring
    :returns: Converted docstring
    :rtype: str
    '''

    result = ''

    for line in docstring.split('\n'):
        if line.startswith("'''"):
            newline = line
        elif line.startswith("' "):
            newline = line[2:]
        elif line.startswith("'"):
            newline = line[1:]
        else:
            newline = line

        result += newline.rstrip() + '\n'

    return result


def find_docstrings(lines):
    '''
    Find all docstrings in the source lines and return them

    :param list lines:
    :returns: All found docstrings
    :rtype: dict {line: docstring}
    '''

    result = OrderedDict()
    indoc = False
    lineno = 0

    for line in lines:
        lineno += 1
        if line == "'''" and not indoc:
            # Start of a new docstring found
            startline, docstring = lineno, line
            indoc = True
        elif line == "'''" and indoc:
            # End of docstring reached
            docstring += '\n' + line
            result[(startline, lineno)] = convert_docstring(docstring)
            indoc = False
        elif indoc:
            # Any line inside of the docstring
            docstring += '\n' + line

    return result


def find_functions(lines, is_class_module=False):
    '''
    Find all function/sub/property declarations in the source lines and return them

    :param list lines:
    :param bool is_class_module: Is this function part of a class module?
    :returns: All found functions
    :rtype: dict {line: function}
    '''
    result = OrderedDict()
    lineno = 0

    for line in lines:
        lineno += 1

        try:
            # Try to parse this line
            vbfunction = convert_function(line, is_class_module)
            result[lineno] = vbfunction

        except ParseException:
            # No problem, there is only no function in this line
            pass

    return result
