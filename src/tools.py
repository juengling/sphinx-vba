'''
Created on 03.12.2019

@author: Christoph.Juengling
'''

from os import listdir
from os.path import join as pjoin, splitext, basename
from re import match, IGNORECASE
import logging

from __init__ import __appname__, __hash__, __version__, __beta__

LOGGING_FORMAT = '%(asctime)s %(levelname)-8s %(name)-22s %(message)s'
LOGGING_DATEFORMAT = '%Y-%m-%d %H:%M:%S'


def init_logging(folder, loglevel=logging.ERROR):
    '''
    Initialise logging to file

    :param str folder: Destination folder for log file
    :param int loglevel: Logging level (if not specified, it defaults to logging.ERROR)
    '''

    logfilename = pjoin(folder, __appname__ + '.log')
    logging.basicConfig(filename=logfilename, filemode='a',
                        format=LOGGING_FORMAT,
                        datefmt=LOGGING_DATEFORMAT,
                        level=loglevel)


def name_version():
    '''
    Return program name and version

    :returns: Program name, version, and hash
    :rtype: str
    '''
    if __beta__:
        return '{} v{}-beta ({})'.format(__appname__, __version__, __hash__)
    else:
        return '{} v{} ({})'.format(__appname__, __version__, __hash__)


def collect_source_files(folder):
    '''
    Collect all files in source folder to be converted

    :param str folder:
    :returns: File name list
    :rtype: list
    '''

    oasis_extensions = ['.def']
    vb_extensions = ['.cls', '.bas', '.frm', '.dsr']

    logger = logging.getLogger('collect_source_files')
    logger.debug('Collecting files from %s', folder)

    result = []
    for file in listdir(folder):
        ext = splitext(file)[1].lower()
        if ext in oasis_extensions:
            prefix, suffix = get_basename(file).split('_')
            if prefix in ['M', 'F']:
                result.append((file, suffix + '.py'))
        elif ext in vb_extensions:
            result.append((file, get_basename(file) + '.py'))

    logger.debug('%d files found', len(result))

    return result


def get_basename(filename):
    '''
    Get basic filename without path and without extension.
    This is more than ``basename`` of os.path will do.

    :param str filename:
    :returns: Basic filename
    :rtype: str
    '''

    return splitext(basename(filename))[0]


def get_file_type(sourcefile):
    '''
    Retrieve some information from the lines, determine the file type, and return it

    :param str sourcefile: Original file name
    :returns: File type
    :rtype: str
    '''

    filetype = 'unknown'
    filename = basename(sourcefile)

    check = [
           # Access/OASIS
           (r'^F_.*\.def$', 'form'),
           (r'^R_.*\.def$', 'report'),
           (r'^M_.*\.def$', 'class-or-module'),

           # VB6
           (r'^.*\.frm$', 'form'),
           (r'^.*\.dsr$', 'report'),
           (r'^.*\.bas$', 'module'),
           (r'^.*\.cls$', 'class')
           ]

    for regex, result in check:
        if match(regex, filename, IGNORECASE):
            filetype = result
            break

    if filetype == 'class-or-module':
        filetype = 'module'
        with open(sourcefile, 'r') as file:
            for line in file.readlines():
                if line.startswith('Attribute VB_Exposed'):
                    filetype = 'class'
                    break

    return filetype


def indentlines(original, indentation):
    '''
    Indent any line in the original string by the indentation string (even it this is '').
    Strip trailing spaces from each line.

    :param str original: Original string
    :param str indentation: Indentation string (!) which will be added to each line
    :returns: Indented string
    :rtype: str
    '''

    result = []
    for line in original.split('\n'):
        result.append((indentation + line).rstrip())

    return '\n'.join(result)
