'''
Vba2sphinx converts parts of any exported VBA file in the given folder
into Python syntax, so that Sphinx can consume them and create a software
documentation set.
'''
from argparse import ArgumentParser
from os.path import join as pjoin
import logging
import sys

from tqdm import tqdm

from __init__ import __appname__, __version__, __hash__, __desc__
from converter import convert_file
from tools import init_logging, name_version, collect_source_files
import public


def main(argv):
    '''
    Vba2sphinx main program

    :param argv: Command line arguments
    :returns: Command line status (Windows: "errorlevel")
    :rtype: int (>= 0)
    '''

    args = parse_command_line(argv)
    public.verbose = args.verbose
    init_logging(args.output, logging.DEBUG)
    logger = logging.getLogger(__appname__)
    logger.info('Starting %s', name_version())

    if public.verbose >= 1:
        print('Convert files in {} to {}'.format(args.folder, args.output))
    filenames = collect_source_files(args.folder)
    for file in tqdm(filenames, unit='files'):
        inputfile = pjoin(args.folder, file[0])
        outputfile = pjoin(args.output, file[1])
        convert_file(inputfile, outputfile)

    return public.EXIT_OK


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments
    :returns: Arguments object
    '''
    parser = ArgumentParser(prog='{name} v{version} (#{hash})'.format(name=__appname__ ,
                                                                    version=__version__,
                                                                    hash=__hash__),
                            description=__desc__)

    parser.add_argument('folder', help='Folder to work with')

    parser.add_argument('output', help='Specify output folder')

    parser.add_argument('-v', '--verbose',
                        action='count', default=0,
                        help='Increase verbosity level')

    args = parser.parse_args(arguments)
    return args


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
