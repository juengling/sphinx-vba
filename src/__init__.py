'''
Created on 16.12.2019

@author: Christoph.Juengling
'''

# Program properties
__appname__ = 'vba2sphinx'
__desc__ = 'Program to convert VBA code to python'
__version__ = '0.1.0'
__beta__ = True
__hash__ = 'DEADC0DE'
