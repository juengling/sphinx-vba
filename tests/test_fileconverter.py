'''
Created on 03.12.2019

@author: Christoph.Juengling
'''
from os import mkdir
from os.path import join as pjoin
from tempfile import TemporaryDirectory
import unittest

from __init__ import __appname__
from converter import convert_file, convert_docstring


#pylint: disable=missing-class-docstring,missing-function-docstring,invalid-name
class Test_Fileconverter(unittest.TestCase):

    def setUp(self):
        # Prepare temporary folder structure
        self._temp = TemporaryDirectory(prefix=__appname__ + '_')
        self._sourcefolder = pjoin(self._temp.name, 'source')
        self._destfolder = pjoin(self._temp.name, 'destination')
        mkdir(self._sourcefolder)
        mkdir(self._destfolder)

    def tearDown(self):
        self._temp.cleanup()
        del self._temp

    def test_convert_file(self):
        content = """' Test: Module
Option Compare Database
Option Explicit


'''
' Just a simple test function
'
' :param str hello: A hello phrase
' :returns: Demo string
' :rtype: String
'''
Public Function mytest(hello As String) As String

mytest = hello & " - heureka!"

End Function
"""

        expected = """def mytest(hello):
    '''
    Just a simple test function

    :param str hello: A hello phrase
    :returns: Demo string
    :rtype: String
    '''

"""

        # Prepare paths
        sourcefile = pjoin(self._temp.name, 'source', 'M_Modul1.def')
        destfile = pjoin(self._temp.name, 'destination', 'Modul1.py')

        # Prepare source file
        with open(sourcefile, 'w') as file:
            file.write(content)

        # Convert the file
        convert_file(sourcefile, destfile)

        # Read the destination file
        with open(destfile, 'r') as file:
            actual = file.read()

        # Compare the result
        self.assertEqual(actual, expected)


class Test_Docstrings(unittest.TestCase):

    def test_convert_docstring(self):

        origin = """'''
' Just a simple test function
'
' :param str hello: A hello phrase
' :returns: Demo string
' :rtype: String
'''
"""
        expected = """'''
Just a simple test function

:param str hello: A hello phrase
:returns: Demo string
:rtype: String
'''

"""

        actual = convert_docstring(origin)
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
