'''
Created on 03.12.2019

@author: Christoph.Juengling
'''
from os.path import join as pjoin
from tempfile import TemporaryDirectory
import unittest

from __init__ import __appname__
from tools import name_version, get_basename, collect_source_files, \
    get_file_type, indentlines
import public


#pylint: disable=missing-class-docstring, missing-function-docstring, invalid-name
class Test_tools(unittest.TestCase):

    nameversionregex = r'^vba2sphinx v\d+\.\d+\.\d+(\-beta)? \([A-F0-9]+\)$'

    def test_name_version_beta(self):
        public.__beta__ = True
        actual = name_version()
        self.assertRegex(actual, self.nameversionregex)

    def test_name_version_nonbeta(self):
        public.__beta__ = False
        actual = name_version()
        self.assertRegex(actual, self.nameversionregex)

    def test_get_basename_path(self):
        original = r'c:\this\is\a\testfile.txt'
        actual = get_basename(original)
        expected = 'testfile'
        self.assertEqual(actual, expected)

    def test_get_basename_nopath(self):
        original = r'testfile.txt'
        actual = get_basename(original)
        expected = 'testfile'
        self.assertEqual(actual, expected)

    def test_get_basename_noextension(self):
        original = r'testfile'
        actual = get_basename(original)
        expected = 'testfile'
        self.assertEqual(actual, expected)

    def test_get_basename_path_noextension(self):
        original = r'c:\this\is\a\testfile'
        actual = get_basename(original)
        expected = 'testfile'
        self.assertEqual(actual, expected)

    def test_get_file_type(self):
        self.assertEqual(get_file_type('F_Something.def'), 'form', 'OASIS form not recognized')
        self.assertEqual(get_file_type('R_Something.def'), 'report', 'OASIS report not recognized')

        self.assertEqual(get_file_type('Something.frm'), 'form', 'VB6 form not recognized')
        self.assertEqual(get_file_type('Something.bas'), 'module', 'VB6 module not recognized')
        self.assertEqual(get_file_type('Something.Dsr'), 'report', 'VB6 report not recognized')

        self.assertEqual(get_file_type('dontknow.not'), 'unknown', 'Unknown file not recognized')

    def test_get_file_type_module(self):
        with TemporaryDirectory(prefix=__appname__) as folder:
            filename = pjoin(folder, 'M_Test.def')
            with open(filename, 'w') as file:
                file.write("""' Test: Module
Option Compare Database
Option Explicit


'''
' Just a simple test function
'
' :param str hello: A hello phrase
' :returns: Demo string
' :rtype: String
'''
Public Function mytest(hello As String) As String

mytest = hello & " - heureka!"

End Function
""")
            self.assertEqual(get_file_type(filename), 'module')

    def test_get_file_type_class(self):
        with TemporaryDirectory(prefix=__appname__) as folder:
            filename = pjoin(folder, 'M_Test.def')
            with open(filename, 'w') as file:
                file.write("""Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Compare Database
Option Explicit

' Test: Class module
""")
            self.assertEqual(get_file_type(filename), 'class')

    def test_indentlines_empty(self):
        original = """
"""

        expected = """
"""

        actual = indentlines(original, '')
        self.assertEqual(actual, expected)

    def test_indentlines_indentby3(self):
        original = 'one\ntwo\nthree\n'
        expected = '   one\n   two\n   three\n'

        actual = indentlines(original, ' ' * 3)
        self.assertEqual(actual, expected)

    def test_indentlines_noindent(self):
        original = 'one\ntwo\nthree\n'
        actual = indentlines(original, '')
        self.assertEqual(actual, original)


class Test_collectsourcefiles(unittest.TestCase):

    def setUp(self):
        # Prepare temporary folder structure
        self._temp = TemporaryDirectory(prefix=__appname__ + '_')
        self._sourcefolder = pjoin(self._temp.name, 'source')
        self._destfolder = pjoin(self._temp.name, 'destination')

    def tearDown(self):
        self._temp.cleanup()
        del self._temp

    def test_collect_source_files_nofiles(self):
        files = collect_source_files(self._temp.name)
        self.assertEqual(len(files), 0)

    def test_collect_source_files_otherfile(self):
        open(pjoin(self._temp.name, 'anything.else'), 'w').close()
        files = collect_source_files(self._temp.name)
        self.assertEqual(len(files), 0)

    def test_collect_source_files_1file(self):
        open(pjoin(self._temp.name, 'M_Module1.def'), 'w').close()

        files = collect_source_files(self._temp.name)

        self.assertEqual(len(files), 1)
        self.assertListEqual(files, [('M_Module1.def', 'Module1.py')])

    def test_collect_source_files(self):
        open(pjoin(self._temp.name, 'M_Module1.def'), 'w').close()
        open(pjoin(self._temp.name, 'Klasse1.cls'), 'w').close()
        open(pjoin(self._temp.name, 'Module2.bas'), 'w').close()
        open(pjoin(self._temp.name, 'Form1.frm'), 'w').close()
        open(pjoin(self._temp.name, 'Report1.Dsr'), 'w').close()

        files = collect_source_files(self._temp.name)

        self.assertEqual(len(files), 5)
        self.assertIn(('M_Module1.def', 'Module1.py'), files)
        self.assertIn(('Klasse1.cls', 'Klasse1.py'), files)
        self.assertIn(('Module2.bas', 'Module2.py'), files)
        self.assertIn(('Form1.frm', 'Form1.py'), files)
        self.assertIn(('Report1.Dsr', 'Report1.py'), files)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
