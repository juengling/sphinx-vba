'''
Created on 02.12.2019

@author: Christoph.Juengling
'''
import unittest
from converter import convert_function


class Test_converter_subs(unittest.TestCase):

    def test_bare_public(self):
        original = 'Public Sub test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_public(self):
        original = 'Public Sub test(one As Integer)'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_public(self):
        original = 'Public Sub test(one As Integer, two As String)'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_bare_private(self):
        original = 'Private Sub test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_private(self):
        original = 'Private Sub test(one As Integer)'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_private(self):
        original = 'Private Sub test(one As Integer, two As String)'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_bare_none(self):
        original = 'Sub test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_none(self):
        original = 'Sub test(one As Integer)'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_none(self):
        original = 'Sub test(one As Integer, two As String)'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
