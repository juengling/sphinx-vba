# Converting VB(A) Syntax into Python

Get the chance to document a VB6/VBA software like a Python program with Sphinx.
To achieve this the VB code must be transformed to valid Python syntax, so Sphinx
can load and analyse it. Because Sphinx only needs the function/class headers and
docstrings, there is no need - though desirable - to convert the whole VB program.

Because Python doesn't distinguish between functions and subroutines by name, there
is no difference in converting them from VB6/VBA to Python.

## Converting Functions and Subs

So these function/sub headers need to be transformed:  

	Function NAME()
	Function NAME() As TYPE
	Public Function NAME()
	Public Function NAME() As TYPE
	-->
	def NAME():
		pass

Private functions (methods) are indicated by a leading underscore in Python.
 
	Private Function NAME()
	Private Function NAME() As TYPE
	-->
	def _NAME():
		pass


ByVal and ByRef don't exist in Python, so that doesn't make a difference.

	Function NAME(param1 As TYPE)
	-->
	def NAME(param1):
		pass

	Function NAME(param1 As TYPE, param2 As TYPE)
	Function NAME(ByVal param1 As TYPE, ByRef param2 As TYPE)
	-->
	def NAME(param1, param2):
		pass

	Function NAME(Optional ByVal param1 As TYPE = INITVAL)
	-->
	def NAME(param1=INITVAL):
		pass

	Function NAME(Optional param1 As TYPE = INITVAL, Optional param2 As TYPE = INITVAL)
	-->
	def NAME(param1=INITVAL, param2=INITVAL):
		pass


Python has no "optional" keyword, but in this case forces a default value, which VB does not.
So if VB defines an optional parameter but does not define any default value, we use
Python's `None` instead.  

	Function NAME(Optional ByVal param1 As TYPE)
	-->
	def NAME(param1=None):
		pass

## Converting Classes

There is no distinct keyword for classes in VB6/VBA. Contrary to Python, VB distinguishes
"Class Modules" from ordinary "Modules" by storing them in another group. The only way
for us to differentiate them by code is to identify the additional code header
(which is invisible in the VBA editor).

### VBA

	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	END
	Attribute VB_Name = "Klasse1"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = False
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = False

### VB 6

	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "clsColor"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = False

Though `VERSION 1.0 CLASS` would be a good identifier, we cannot rely on the existance of it,
because when the VBA code is exported with OASIS, only the `Attribute` section exists.

### Converting to Python

To create a class in Python, we need to extract the file name and use it as Python's class name.

	Klasse1.cls (internal export)
	M_Klasse1.def (OASIS export)
	-->
	class Klasse1():
		pass

Because VB6 and VBA don't support inheritance, there is no need to deal with.
